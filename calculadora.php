<?php
   class Calculadora{
       // atributos
       public $nro1;
       public $nro2;
       // metodos u operaciones
       public function Sumar()
       {
           return $this->nro1 + $this->nro2;
       }

       public function Restar()
       {
           return $this->nro1 - $this->nro2;
       }

       public function Multiplicar()
        {
            return $this->nro1 * $this->nro2;
        }

       public function Dividir()
       {
            if($nro2 == 0)
           {
            return $this->nro1 / $this->nro2;
           }
            else
           {
            return "No se puede dividr entre 0";
           }
       }

       private function Fact($nro)
        {
            if($nro == 0)
                return 1;
            else
                return $nro * $this->Fact($nro - 1);
        }

        public function Factorial()
        {
            return $this->Fact($this->nro1);
        }

        private function Pote($base, $exp)
        {
            if($exp == 0)
            {
                return 1;
            }
            else{
                return $base * $this->Pote($base , $exp - 1);
            } 
        }

       public function Potencia()
       {
           return $this->Pote($this->nro1,$this->nro2);
       }

       private function Sen($nro)
       {
           return (sin(deg2rad($nro)));
       }

       public function Seno()
       {
           return $this->Sen($this->nro1);
       }

       private function Cose($nro) 
       {
           return (cos(($nro * pi()) / 180));
       }

       public function Coseno()
       {
           return $this->Cose($this->nro1);
       }

       private function Tang($nro)
       {
           return (tan(($nro * pi()) / 180));
       }

       public function Tangente()
       {
           return $this->Tang($this->nro1);
       }

        public function Inverso()
        {
            return (1/$this->nro1);
        }

        public function Porcentaje()
        {
            return ($this->nro1*$this->nro2/100);
        }

        public function RaizCuadrada()
        {
            return (sqrt($this->nro1));
        }

        public function RaizNesima()
         {
            return (pow($this->nro1, 1/$this->nro2));
         }

   }
?>