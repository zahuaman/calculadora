<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Procesar PHP dentro del mismo Formulario</title>
    <title>Bootstrap 5!</title>
    <link rel="stylesheet" href="estilo.css" >
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
 
</head>

<body>
    <h1>❤CALCULADORA❤</h1>
<form action="#" method="POST" >
        <p>Numero 01: <input type="text" name="txtNro1"/></p>
        <p>Numero 02: <input type="text" name="txtNro2"/></p>
    
<input type="submit" name="btnSumar" style="font-size:30px; color:#000000; text-align:center; " value="+"/>
<input type="submit" name="btnRestar" style="font-size:30px; color:#000000; text-align:center; " value="-"/>
<input type="submit" name="btnMultiplicar" style="font-size:30px; color:#000000; text-align:center; " value="*"/>
<input type="submit" name="btnDividir" style="font-size:30px; color:#000000; text-align:center; " value="/"/>
<p></p><input type="submit" name="btnFactorial" style="font-size:30px; color:#000000; text-align:center; " value="!"/>
<input type="submit" name="btnPotencia" style="font-size:30px; color:#000000; text-align:center; " value="^"/>
<input type="submit" name="btnInverso" style="font-size:30px; color:#000000; text-align:center; " value="1/x"/>
<input type="submit" name="btnPorcentaje" style="font-size:30px; color:#000000; text-align:center; " value="%"/>
<p></p><input type="submit" name="btnRaizCuadrada" style="font-size:30px; color:#000000; text-align:center; " value="√x"/>
<input type="submit" name="btnRaizNesima" style="font-size:30px; color:#000000; text-align:center; " value="°√x"/>
<input type="submit" name="btnSeno" style="font-size:30px; color: #000000; text-align:center; " value="Sen"/>
<input type="submit" name="btnCoseno" style="font-size:30px; color:#000000; text-align:center; " value="Cos"/>
<p></p><input type="submit" name="btnTangente" style="font-size:30px; color:#000000; text-align:center; " value="Tan"/>
 

        </p>
    </form>
<?php
if(isset($_POST))
{
    // Llamar a la clase calculadora 
    include("calculadora.php");
    $nro1 = $_POST['txtNro1'];
    $nro2 = $_POST['txtNro2'];
    if(isset($_POST['btnSumar']))
    {
        // Instanciar un objeto a traves de la clase
        $calculo = new Calculadora;
        $calculo->nro1 = $nro1;
        $calculo->nro2 = $nro2;
        $suma = $calculo->Sumar();
        echo "La suma de los numeros es: ", $suma;
    }
    else if(isset($_POST['btnRestar']))
    {
        $calculo = new Calculadora;
        $calculo->nro1 = $nro1;
        $calculo->nro2 = $nro2;
        $resta = $calculo->Restar();
        echo "La resta de los numeros es: ", $resta;
    }
    if(isset($_POST['btnMultiplicar']))
    {
        $calculo = new Calculadora;
        $calculo->nro1 = $nro1;
        $calculo->nro2 = $nro2;
        $multiplica = $calculo->Multiplicar();
        echo "La multiplicacion de los numeros es: ", $multiplica;
    }
    else if(isset($_POST['btnDividir']))
    {
        $calculo = new Calculadora;
        $calculo->nro1 = $nro1;
        $calculo->nro2 = $nro2;
        $divide = $calculo->Dividir();
        echo "La division de los numeros es: ", $divide;
    }
    else if(isset($_POST['btnFactorial']))
    {
        $calculo = new Calculadora;
        $calculo->nro1 = $nro1;
        $calculo->nro1 = $nro1;
        $fact = $calculo->Factorial();
        echo "El factorial del numero es: ", $fact;
    }
    else if(isset($_POST['btnPotencia']))
    {
        $calculo = new Calculadora;
        $calculo->nro1 = $nro1;
        $calculo->nro2 = $nro2;
        $pote = $calculo->Potencia();
        echo "La potencia de los numeros es: ", $pote;
    }
    else if(isset($_POST['btnSeno']))
    {
        $calculo = new Calculadora;
        $calculo->nro1 = $nro1;
        $calculo->nro2 = $nro2;
        $sen= $calculo->Seno();
        echo "El seno del numero es: ", $sen;
    }
    else if(isset($_POST['btnCoseno']))
    {
        $calculo = new Calculadora;
        $calculo->nro1 = $nro1;
        $calculo->nro2 = $nro2;
        $cose = $calculo->Coseno();
        echo "El coseno del numero es: ", $cose;
    }
    else if(isset($_POST['btnTangente']))
    {
        $calculo = new Calculadora;
        $calculo->nro1 = $nro1;
        $calculo->nro2 = $nro2;
        $tang = $calculo->Tangente();
        echo "La tangente del numero es: ", $tang;
    }
    else if(isset($_POST['btnInverso']))
    {
        $calculo = new Calculadora;
        $calculo->nro1 = $nro1;
        $calculo->nro2 = $nro2;
        $inverso = $calculo->Inverso();
        echo "El numero inverso es: ", $inverso;
    }
    else if(isset($_POST['btnPorcentaje']))
    {
        $calculo = new Calculadora;
        $calculo->nro1 = $nro1;
        $calculo->nro2 = $nro2;
        $porcentaje = $calculo->Porcentaje();
        echo "El porcentaje del numero es: ", $porcentaje;
    }
    else if(isset($_POST['btnRaizCuadrada']))
    {
        $calculo = new Calculadora;
        $calculo->nro1 = $nro1;
        $calculo->nro2 = $nro2;
        $raiz = $calculo->RaizCuadrada();
        echo "La raiz cuadrada del numero es: ", $raiz;
    }
    else if(isset($_POST['btnRaizNesima']))
    {
        $calculo = new Calculadora;
        $calculo->nro1 = $nro1;
        $calculo->nro2 = $nro2;
        $raizNe = $calculo->RaizNesima();
        echo "La raiz nesima del numero es: ", $raizNe;
    }
}
?>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    
   
</body>
</html>
				


				
    
